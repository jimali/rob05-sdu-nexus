This project contain sources for a mobile platform used in the robotics course ROB05 
at the university of southern denmark. 

The mobile platform is based on the 2 wheel drive nexus robot platform with some 
extensions. These include:
- 2d laser range scanner Hokuyo URG
- 7 Ah lead acid battery
- Raspberry Pi2 main board with wifi

as such this repository contains:
- code for the arduino based nexus main board
- code for the rasp pi 2 main board that interface to the arduino and the hokuyo
- CAD files for mounting the extensions to the Nexus platform

